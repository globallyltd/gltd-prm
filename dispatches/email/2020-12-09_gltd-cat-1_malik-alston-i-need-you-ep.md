<!-----
template: press-release
email:
    {% if 'dj' in tags %}
    subject: "[DJ PROMO] Malik Alston - I Need You EP (gltd.cat/1)"
    {% else %}
    subject: "Malik Alston - I Need You EP (gltd.cat/1)"
    {% endif %}
    from_email:
    - addr: hey@gltd.email 
      name: Globally Limited
    to_email:
    - addr: {{ email_main }}
      {% if name_first %}
      name: {{ name_first }} {% if name_last %} {{ name_last }} {% endif %}
      {% elif name_org %}
      name: {{ name_org }}
      {% endif %}
assets:
    press_photo: 
      path: 1_malik-alston-i-need-you-ep/malik-alston-press-photo.png
      url: https://gltd-prm-assets.nyc3.digitaloceanspaces.com/1_malik-alston-i-need-you-ep/malik-alston-press-photo.png
      expires: null
    promo:
      path: 1_malik-alston-i-need-you-ep/malik-alston-i-need-you-ep.zip
      url: https://gltd-prm-assets.nyc3.digitaloceanspaces.com/1_malik-alston-i-need-you-ep/malik-alston-i-need-you-ep.zip
      expires: null
    cover:
      path: 1_malik-alston-i-need-you-ep/malik-alston-i-need-you-ep-album-cover.jpg
      url: https://gltd-prm-assets.nyc3.digitaloceanspaces.com/1_malik-alston-i-need-you-ep/malik-alston-i-need-you-ep-album-cover.jpg
      expires: null
    banner:
      path: _core/logo-banner.png
      url: https://gltd-prm-assets.nyc3.digitaloceanspaces.com/_core/logo-banner.png
      expires: null
---->

<center> 
<a href="https://globally.ltd">
  <img src="{{ assets['banner']['url'] }}"></img>
</a> 
</center>

{% if name_first and 'homie' in tags %}
Whatsup {{ name_first }}!
{% elif name_first %}
Hey {{ name_first }}!
{% else %}
Hey friend!
{% endif %}

I hope this finds you comfy and warm.

I wanted to share with you [Globally Ltd.'s](https://globally.ltd) first release, the I Need You EP from Detroit's Malik Alston We're really excited about it and sure hope you enjoy! 🎧

{% if 'dj' in tags and 'press' not in tags %}

You can [download a promo here]({{ assets['promo']['url'] }}). Let us know what you think and if you include it in a mix ;-0

You can also find Malik's record here:

{% elif 'press' in tags %}

You can [download the record here]({{ assets['promo']['url'] }}). Let us know what you think! We'd love to have {% if name_org %} {{ name_org }} {% else %} yall {% endif %} feature it ;-0

You can also find Malik's release here:

{% else %}

Find Malik here:

{% endif %}


- [Bandcamp](https://gltd.store/album/i-need-you-ep/)
- [Spotify](https://api.ffm.to/sl/e/c/mam0rbr?cd=eyJ1YSI6eyJ1YSI6Ik1vemlsbGEvNS4wIChNYWNpbnRvc2g7IEludGVsIE1hYyBPUyBYIDEwLjE1OyBydjo4My4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94LzgzLjAiLCJicm93c2VyIjp7Im5hbWUiOiJGaXJlZm94IiwidmVyc2lvbiI6IjgzLjAiLCJtYWpvciI6IjgzIn0sImVuZ2luZSI6eyJuYW1lIjoiR2Vja28iLCJ2ZXJzaW9uIjoiODMuMCJ9LCJvcyI6eyJuYW1lIjoiTWFjIE9TIiwidmVyc2lvbiI6IjEwLjE1In0sImRldmljZSI6e30sImNwdSI6e319LCJjbGllbnQiOnsicmlkIjoiZGYxNmUwMzktMjc3MC00ZDE3LWJmMmMtZDNlYWJmODM5MWFkIiwic2lkIjoiZjI0YjAwZTMtYzMzNy00YzA1LTgzMzQtOTBiNzgyYjBkNjgxIiwiaXAiOiIyNC4xOTEuMTUuMjMyIiwicmVmIjoiIiwiaG9zdCI6ImZpZnRlZW5wbS5mZm0udG8iLCJsYW5nIjoiZW4tVVMiLCJpcENvdW50cnkiOiJVUyJ9LCJpc0Zyb21FVSI6ZmFsc2UsImNvdW50cnlDb2RlIjoiVVMiLCJpZCI6IjVmZDZiYjAxM2MwMDAwMGUwMDQ0NjliYyIsInR6byI6MzAwLCJjaCI6bnVsbCwiYW4iOm51bGwsImRlc3RVcmwiOiJodHRwczovL29wZW4uc3BvdGlmeS5jb20vYWxidW0vMXNkMWVIOGJNaDVidjNKQnVVbUhvOSIsInZpZCI6IjY5ODUyMjA1LTc2YTEtNDZlZi05MjQwLTVmYTVjYzYyMjU2ZSIsInNydmMiOiJzcG90aWZ5IiwicHJvZHVjdCI6InNtYXJ0bGluayIsInNob3J0SWQiOiJtYW0wcmJyIiwiaXNBdXRob3JpemF0aW9uUmVxdWlyZWQiOmZhbHNlLCJvd25lciI6IjVmMWI2Yzg5MzAwMDAwNWY5OWNmZjYzMCIsImFyIjoiNWYxYjYxMjgyNDAwMDBmMzA3YThhZWIwIiwiaXNTaG9ydExpbmsiOmZhbHNlfQ)
- [Apple Music](https://api.ffm.to/sl/e/c/mam0rbr?cd=eyJ1YSI6eyJ1YSI6Ik1vemlsbGEvNS4wIChNYWNpbnRvc2g7IEludGVsIE1hYyBPUyBYIDEwLjE1OyBydjo4My4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94LzgzLjAiLCJicm93c2VyIjp7Im5hbWUiOiJGaXJlZm94IiwidmVyc2lvbiI6IjgzLjAiLCJtYWpvciI6IjgzIn0sImVuZ2luZSI6eyJuYW1lIjoiR2Vja28iLCJ2ZXJzaW9uIjoiODMuMCJ9LCJvcyI6eyJuYW1lIjoiTWFjIE9TIiwidmVyc2lvbiI6IjEwLjE1In0sImRldmljZSI6e30sImNwdSI6e319LCJjbGllbnQiOnsicmlkIjoiZGYxNmUwMzktMjc3MC00ZDE3LWJmMmMtZDNlYWJmODM5MWFkIiwic2lkIjoiZjI0YjAwZTMtYzMzNy00YzA1LTgzMzQtOTBiNzgyYjBkNjgxIiwiaXAiOiIyNC4xOTEuMTUuMjMyIiwicmVmIjoiIiwiaG9zdCI6ImZpZnRlZW5wbS5mZm0udG8iLCJsYW5nIjoiZW4tVVMiLCJpcENvdW50cnkiOiJVUyJ9LCJpc0Zyb21FVSI6ZmFsc2UsImNvdW50cnlDb2RlIjoiVVMiLCJpZCI6IjVmZDZiYjAxM2MwMDAwMGUwMDQ0NjliYyIsInR6byI6MzAwLCJjaCI6bnVsbCwiYW4iOm51bGwsImRlc3RVcmwiOiJodHRwczovL2dlby5tdXNpYy5hcHBsZS5jb20vdXMvYWxidW0vaS1uZWVkLXlvdS1lcC8xNTM4MjE1MjA2P3VvPTQmYXBwPW11c2ljJmN0PUZGTV84MjNjMjQ4ZWEyZmUxMDM1ZGY5YmNlN2UyOTAwOGQ2MyZhdD0xMDEwbFNkMiIsInZpZCI6IjY5ODUyMjA1LTc2YTEtNDZlZi05MjQwLTVmYTVjYzYyMjU2ZSIsInNydmMiOiJhcHBsZSIsInByb2R1Y3QiOiJzbWFydGxpbmsiLCJzaG9ydElkIjoibWFtMHJiciIsImlzQXV0aG9yaXphdGlvblJlcXVpcmVkIjpmYWxzZSwib3duZXIiOiI1ZjFiNmM4OTMwMDAwMDVmOTljZmY2MzAiLCJhciI6IjVmMWI2MTI4MjQwMDAwZjMwN2E4YWViMCIsImlzU2hvcnRMaW5rIjpmYWxzZX0)

------

<center> 

*Malik Alston is a Detroit musician and producer to the core. Having grown up in a household with a classically-trained vocalist/pianist mother and a father with a strong penchant for record collecting, Malik was quick to participate in local church and community choirs at a young age— quickly forging a need to find a voice of his own within the world of songwriting. With the goal of taking his gospel, jazz, R&B, funk, and soul tunes even further, a young Malik soon picked up the Yamaha RY30 drum machine (per the recommendation of his childhood friend Javonntte) to accompany the piano in his childhood home. It was also around this time that Malik found new inspiration on the dancefloor, where the sounds of "progressive" dance music began to dominate the local sound-systems in the mid-80s. Even Malik's sister, Mareitta (who is also an accomplished musician in her own right), would take him to parties at her university, where the DJs continued to strengthen his spiritual connection to music. Malik went on to study electrical engineering at university, only to throw himself completely into music soon thereafter with the formation of his Industry One Studio (with Javonntte) and strong commitment to the dance. It's this unique combination of formed experience that has allowed Malik to assume his singular vision as singer, musician, producer, dancer, collaborator, and underground pioneer.*

<br/>

*It's with endless gratitude and respect that Globally Limited can celebrate its first release with four new club-ready tracks from Malik Alston: The I Need You EP. An ode to the boundless love he has for his wife, Badriyyah, the EP is a positive and dynamic collection of house and future-jazz music that is as joyful as it is virtuosic. Malik's masterful approach to composition and production comes through immediately upon first listen, delivered with both an earnest and playful grin. It's as though we're being treated to the sound of a maverick artist take his sound even further— fully complete with an assured sense that there's no intention of slowing down! The EP's opener "Halftime", with its arpeggiated bassline and intoxicating organ, is a cut that keeps you hanging on every moment. Track 3, "In the Dance", takes the joyfulness even further with dubbed-out vocal samples and irresistible hand percussion. But where Malik really shines here is in his ability to accompany his productions with his remarkable voice. On "I Need You", the EP's closer and title track, it becomes evident that Malik is truly a timeless artist who can do it all. And having collaborated with artists and friends like Craig Huckaby, Alton Miller, Amp Fiddler, Roy Davis Jr., and Billy Love (to name a few) over the years, it's becoming increasingly more evident that Malik's deep respect for the world of soulful music has been reflected back upon him.*
<br/>
<br/>
<p>
<a href="https://gltd.store/album/i-need-you-ep/">
  <img src="{{ assets['cover']['url'] }}" width="300px"></img>
</a>
</p>
</center>

------
[GLTD.CAT/0](https://gltd.cat/0)

**BC** - [gltd.store](https://gltd.store)

**SC** - [soundcloud.com/globallyltd](https://soundcloud.com/globallyltd)

**IG** - [instagram.com/globally.ltd](https://instagram.com/globally.ltd)

**FB** - [facebook.com/globallyltd](https://facebook.com/globallyltd)
