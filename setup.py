import os
from setuptools import setup, find_packages

reqs = os.path.abspath(os.path.join(os.path.dirname(__file__), "requirements.txt"))
with open(reqs) as f:
    install_requires = [
        req.strip().split("==")[0] for req in f if not req.startswith("-e")
    ]

config = {
    "name": "gltd-prm",
    "version": "0.1",
    "packages": find_packages(),
    "install_requires": install_requires,
    "entt" "author": "gltd",
    "author_email": "hey@gltd.email",
    "description": "Person|Press|Producer Relationship Manager using Notion + custom python code.",
    "entry_points": {"console_scripts": ["prm-dispatch=gltd_prm.cli:send_dispatch"]},
}

setup(**config)
