import os
from pathlib import Path

import yaml
from dotenv import load_dotenv

from gltd_prm import utils

#############
# INTERNALS #
#############

# default directories
DEFAULT_TEMPLATES_DIR = utils.here(__file__, "defaults/templates/")
DEFAULT_MACROS_DIR = utils.here(__file__, "defaults/macros/")
DEFAULT_ASSETS_DIR = utils.here(__file__, "defaults/assets/")

#############
#  CONFIGS  #
#############
# config file
CONFIG = yaml.safe_load(
    open(
        os.path.expanduser(
            os.getenv("GLTD_PRM_CONFIG_FILE", utils.here(__file__, "..", "config.yml"))
        )
    )
)
# # project settings
BASE_DIR = os.path.expanduser(CONFIG["prj"]["base_dir"])
ENV_PREFIX = CONFIG["prj"].get("env_prefix", utils.DEFAULT_ENV_PREFIX)
ENV_FILE = utils.get_prj_config_path(BASE_DIR, CONFIG["prj"].get("env_file", ".env"))
load_dotenv(ENV_FILE)


# instatiate config
CONFIG = utils.load_config(CONFIG, ENV_PREFIX, BASE_DIR)


# project directories
DISPATCHES_DIR = os.path.join(BASE_DIR, CONFIG["prj"]["dispatches"]["dir"])
TEMPLATES_DIR = os.path.join(BASE_DIR, CONFIG["prj"]["templates"]["dir"])
CACHE_DIR = os.path.join(BASE_DIR, CONFIG["prj"]["cache"]["dir"])
MACROS_DIR = os.path.join(BASE_DIR, CONFIG["prj"]["macros"]["dir"])
ASSETS_DIR = os.path.join(BASE_DIR, CONFIG["prj"]["assets"]["dir"])

###############
#  PLATFORMS  #
###############

# notion
PLATFORM_NOTION_TOKEN_V2 = CONFIG["platforms"]["notion"]["config"]["token_v2"]

# email
PLATFORM_EMAIL_FROM_USER = CONFIG["platforms"]["email"]["config"]["from_user"]
PLATFORM_EMAIL_FROM_USER_FULL_NAME = CONFIG["platforms"]["email"]["config"][
    "from_user_full_name"
]
PLATFORM_EMAIL_PASSWORD = CONFIG["platforms"]["email"]["config"]["password"]
PLATFORM_EMAIL_HOST = CONFIG["platforms"]["email"]["config"]["host"]
PLATFORM_EMAIL_SMTP_PORT = CONFIG["platforms"]["email"]["config"]["smtp_port"]
PLATFORM_EMAIL_IMAP_PORT = CONFIG["platforms"]["email"]["config"]["imap_port"]

# slack
PLATFORM_SLACK_WEBHOOK_URL = CONFIG["platforms"]["slack"]["config"]["webhook_url"]

# do-assets
PLATFORM_DO_ASSETS_SPACE = CONFIG["platforms"]["do_assets"]["config"]["space"]
PLATFORM_DO_ASSETS_REGION_NAME = CONFIG["platforms"]["do_assets"]["config"][
    "region_name"
]
PLATFORM_DO_ASSETS_ENDPOINT_URL = CONFIG["platforms"]["do_assets"]["config"][
    "endpoint_url"
]
PLATFORM_DO_ASSETS_BASE_URL = CONFIG["platforms"]["do_assets"]["config"]["base_url"]
PLATFORM_DO_ASSETS_ACCESS_ID = CONFIG["platforms"]["do_assets"]["config"]["access_id"]
PLATFORM_DO_ASSETS_SECRET_KEY = CONFIG["platforms"]["do_assets"]["config"]["secret_key"]

###############
#    PRM      #
###############

# prm
PRM_PEOPLE_HOST_PLATFORM = CONFIG["prm"]["people"]["host_platform"]
PRM_PEOPLE_URL = CONFIG["prm"]["people"]["url"]
# PRM_ACTIVITY_EMAIL_CONFIG = list(map(utils.get_activity_email_config, CONFIG['prm']['events']['email']))
