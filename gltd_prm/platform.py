"""
Connections to services:
    - notion 
    - slack
    - email
"""

import smtplib
import datetime
import urllib.parse
from contextlib import contextmanager
from email.utils import make_msgid
from email.message import EmailMessage
from email.headerregistry import Address

from boto3 import session
from botocore.client import Config
from notion.client import NotionClient

from gltd_prm import utils
from gltd_prm.config import (
    PLATFORM_EMAIL_HOST,
    PLATFORM_EMAIL_FROM_USER,
    PLATFORM_EMAIL_FROM_USER_FULL_NAME,
    PLATFORM_EMAIL_PASSWORD,
    PLATFORM_EMAIL_IMAP_PORT,
    PLATFORM_EMAIL_SMTP_PORT,
    PLATFORM_NOTION_TOKEN_V2,
    PLATFORM_SLACK_WEBHOOK_URL,
    PLATFORM_DO_ASSETS_SPACE,
    PLATFORM_DO_ASSETS_REGION_NAME,
    PLATFORM_DO_ASSETS_ENDPOINT_URL,
    PLATFORM_DO_ASSETS_BASE_URL,
    PLATFORM_DO_ASSETS_ACCESS_ID,
    PLATFORM_DO_ASSETS_SECRET_KEY,
)

class Platform(object):
    @contextmanager
    def connect(self, *args, **kwargs):
        raise NotImplementedError


class Email(Platform):
    def __init__(self, **kwargs):

        # from email
        self.from_user_email = kwargs.get("from_user", PLATFORM_EMAIL_FROM_USER)
        self.from_user_full_name = kwargs.get(
            "from_user_full_name", PLATFORM_EMAIL_FROM_USER_FULL_NAME
        )
        self.from_user_name, self.domain = utils.email_parse(self.from_user_email)

        self.password = kwargs.get("password", PLATFORM_EMAIL_PASSWORD)
        self.host = kwargs.get("host", PLATFORM_EMAIL_HOST)
        self.smtp_port = kwargs.get("smtp_port", PLATFORM_EMAIL_SMTP_PORT)
        self.imap_port = kwargs.get("imap_port", PLATFORM_EMAIL_IMAP_PORT)

        # cache connections
        self._smtp = smtplib.SMTP(self.host, port=self.smtp_port)
        self._smtp.login(user=self.from_user_email, password=self.password)
        self._imap = None

    # utils

    def _get_address(self, addr: str, name: str = "") -> Address:
        """
        Parse an email to an address field
        """
        user_name, domain = utils.email_parse(addr)
        return Address(name, user_name, domain)

    # connectors

    @property
    def smtp(self):
        """
        The SMTP connection
        ```
        m = Mailer()
        with m.smtp() as connection:
            connection.send_message('blah')
        ```
        """
        return self._smtp 

    @contextmanager
    def imap(self):
        raise NotImplementedError

    # message

    @property
    def default_from_address(self) -> Address:
        """
        The address to send the email from.
        """
        return [Address(self.from_user_full_name, self.from_user_name, self.domain)]

    def create_message(self, **kwargs: dict) -> EmailMessage:
        """
        Create an Email message
        See: https://docs.python.org/3/library/email.message.html#email.message.EmailMessage
        """
        msg = EmailMessage()
        msg_cid = make_msgid()

        # subject
        msg["Subject"] = kwargs.get("subject")

        # from addresses
        # from_addr = [self.default_from_address]
        from_addr = [self._get_address(**e) for e in kwargs.get("from_email", [])]
        msg["From"] = from_addr

        # to addresses
        to_addr = [self._get_address(**e) for e in kwargs.get("to_email", [])]
        msg["To"] = to_addr

        # TODO: cc/bcc

        # content
        msg.set_content(kwargs.get("text", ""))
        _html_content = kwargs.get("html", kwargs.get("text", ""))
        msg.add_alternative(_html_content, subtype="html")

        # TOOD: attachements
        return msg

    def send(self, **kwargs):
        """
        """
        self.smtp.send_message(self.create_message(**kwargs))

    def search_inbox(self, query: str):
        """
        :param query: A query to use to search the inbox.
        """
        return []


class Notion(Platform):
    def __init__(self, token_v2=PLATFORM_NOTION_TOKEN_V2):
        # Obtain the `token_v2` value by inspecting your browser cookies on a logged-in (non-guest) session on Notion.s
        self.token_v2 = token_v2
        self.conn = NotionClient(self.token_v2)

    # GET METHODS #

    def get_dataset(self, url):
        """
        :param url:
        """
        cv = self.conn.get_block(url)
        cv.refresh()
        fields = [s["slug"] for s in cv.collection.get_schema_properties()]

        for row in cv.collection.get_rows():
            out = {}
            for f in fields:
                try:
                    out[f] = getattr(row, f, None)
                except TypeError:
                    out[f] = None
            yield out

class DOAssets(Platform):

    space = PLATFORM_DO_ASSETS_SPACE
    base_url = PLATFORM_DO_ASSETS_BASE_URL

    def __init__(self):
        self.connect()

    def connect(self):
        self.session = session.Session()
        self.conn = self.session.client(
            "s3",
            region_name=PLATFORM_DO_ASSETS_REGION_NAME,
            endpoint_url=PLATFORM_DO_ASSETS_ENDPOINT_URL,
            aws_access_key_id=PLATFORM_DO_ASSETS_ACCESS_ID,
            aws_secret_access_key=PLATFORM_DO_ASSETS_SECRET_KEY,
        )

    def upload(self, from_, to_, expires=None):
        """
        Upload a file to DO spaces and return the url
        """
        args = {}
        if not expires:
            args["ACL"] = "public-read"
        else:
            args["Expires"] = datetime.datetime.now() + datetime.timedelta(days=expires)

        self.conn.upload_file(from_, self.space, to_, ExtraArgs=args)
        return urllib.parse.urljoin(self.base_url, to_)
