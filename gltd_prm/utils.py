import os
import re
from typing import Tuple, Any
import collections.abc
from html.parser import HTMLParser
from html.entities import name2codepoint

import yaml
from markdown import markdown


def here(fileobj: object, *paths) -> str:
    """
    Get the current directory and absolute path of a __file__.
    :param filobj: the ``__file__`` metavar
    :param paths: a list of path sections to pass to ``os.path.join``
    :return str
    """
    return os.path.abspath(os.path.join(os.path.dirname(fileobj), *paths))


def email_parse(raw_email: str) -> Tuple[str, str]:
    """
    Return the user_name and email_domain from a raw email address.
    """
    return raw_email.split("@")


DEFAULT_ENV_PREFIX = "GLTD_PRM_"


def get_secret(
    env_str: str,
    env_type: type = str,
    env_prefix: str = DEFAULT_ENV_PREFIX,
    default: Any = None,
) -> Any:
    """
    Get an env variable formatted like: "$GLTD_PRM_CONFIG_VAR"
    :param env_str: The environment variable, with an optionally ommitted `prefix`
    :param env_type: The variable type, eg: str, int, float, bool, etc
    :param env_prefix: An optional prefix to provide to put in front of `env_str`
    :param default: The default value if not varible is provided
    """
    # format env_str
    if env_str.startswith("$"):
        env_str = env_str[1:]
    if env_prefix is not None and not env_str.startswith(env_prefix):
        env_str = f"{env_prefix}_{env_str.upper()}"
    env_val = os.getenv(env_str, default)

    # load
    return env_type(env_val)


def get_prj_config_path(prj_dir: str, path: str) -> str:
    """
    :param prj_dir: The base directory for a PRM project
    :param path: The path to project collection (assets, macros, dispatches, templates)
    """
    if not path.startswith("/"):
        return os.path.join(prj_dir, path)
    return path


def get_activity_email_config(cfg: dict, env_prefix: str = DEFAULT_ENV_PREFIX) -> dict:
    """
    Get the password for an activity email configuration
    """
    cfg["password"] = get_secret(cfg["password"], env_prefix=env_prefix)
    return cfg


def load_config(d: dict, env_prefix: str, base_dir: str) -> dict:
    """"""
    out = {}
    for k, v in d.items():
        if isinstance(v, str) and v.startswith("$"):
            out[k] = get_secret(v, env_prefix=env_prefix)
        elif isinstance(v, str) and v.endswith(".yml"):
            out[k] = yaml.safe_load(open(os.path.join(base_dir, v)))
        elif isinstance(v, collections.abc.Mapping):
            out[k] = load_config(d.get(k, {}), env_prefix, base_dir)
        else:
            out[k] = v
    return out


def md_to_html(md):
    return markdown(md)


def lmap(l, fx):
    return list(map(l, fx))


# HTML <-> text conversions.


class _HTMLToText(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self._buf = []
        self.hide_output = False

    def handle_starttag(self, tag, attrs):
        if tag in ("p", "br") and not self.hide_output:
            self._buf.append("\n")
        elif tag in ("script", "style"):
            self.hide_output = True

    def handle_startendtag(self, tag, attrs):
        if tag == "br":
            self._buf.append("\n")

    def handle_endtag(self, tag):
        if tag == "p":
            self._buf.append("\n")
        elif tag in ("script", "style"):
            self.hide_output = False

    def handle_data(self, text):
        if text and not self.hide_output:
            self._buf.append(re.sub(r"\s+", " ", text))

    def handle_entityref(self, name):
        if name in name2codepoint and not self.hide_output:
            c = unichr(name2codepoint[name])
            self._buf.append(c)

    def handle_charref(self, name):
        if not self.hide_output:
            n = int(name[1:], 16) if name.startswith("x") else int(name)
            self._buf.append(unichr(n))

    def get_text(self):
        return re.sub(r" +", " ", "".join(self._buf))


def html_to_text(html):
    """
    Given a piece of HTML, return the plain text it contains.
    This handles entities and char refs, but not javascript and stylesheets.
    """
    parser = _HTMLToText()
    parser.feed(html)
    parser.close()
    return parser.get_text().strip()
