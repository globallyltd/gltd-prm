import re
import os
import time
import random
from copy import copy
from typing import List, Dict
from contextlib import contextmanager

import yaml
from jinja2 import Template

from gltd_prm import exc, utils, platform
from gltd_prm.config import (
    DEFAULT_ASSETS_DIR,
    DEFAULT_MACROS_DIR,
    DEFAULT_TEMPLATES_DIR,
    CACHE_DIR,
    TEMPLATES_DIR,
    MACROS_DIR,
    DISPATCHES_DIR,
    ASSETS_DIR,
    PRM_PEOPLE_HOST_PLATFORM,
    PRM_PEOPLE_URL,
    CONFIG,
)


SENT_TO = [
    {"addr": "hiddenxprism@gmail.com", "name": "Dylan"},
    {"addr": "coms@20jazzfunkgreats.co.uk", "name": "20 jazz funk greats"},
    {"addr": "europe@aagoo.com", "name": "Aagoo"},
    {"addr": "a.aua@gmx.de", "name": "ABEL AUER"},
    {"addr": "lost@aboutblank.li", "name": "about blank"},
    {"addr": "russell.el.butler@gmail.com", "name": "Russell"},
    {"addr": "info@adhoc.fm", "name": "Ad hoc"},
    {"addr": "emilie@adhoc.fm", "name": "Emilie"},
    {"addr": "alec@aagoo.com", "name": "Alec"},
    {"addr": "joe@artonair.org", "name": "Joe"},
    {"addr": "alleveryoneunited@gmail.com", "name": "All Everyone United"},
    {"addr": "bucky@animalnewyork.com", "name": "Animal"},
    {"addr": "contact@annualartmagazine.com", "name": "Annual Magazine"},
    {"addr": "dustin@anthemmagazine.com", "name": "Anthem Magazine"},
    {"addr": "nik@anthemmagazine.com", "name": "Anthem Magazine"},
    {"addr": "kee@anthemmagazine.com", "name": "Anthem Magazine"},
    {"addr": "artfagcity@gmail.com", "name": "Art Fag City"},
    {"addr": "info@artobserved.com", "name": "Art Observed"},
    {"addr": "info@artpapers.org", "name": "Art Papers"},
    {"addr": "info@badbonn.ch", "name": "bad bonn"},
    {"addr": "im@beatsinspace.net", "name": "Tim"},
    {"addr": "benjamin.lee@theguardian.com", "name": "Bill"},
    {"addr": "info@beyondbeyondisbeyond.com", "name": "Beyond Beyond is Beyond"},
    {"addr": "info@bidoun.org", "name": "Bidoun"},
    {"addr": "music@blahblahblahscience.com", "name": "Blah Blah Blah Science"},
    {"addr": "space@blocorganisation.com", "name": "bloc"},
    {"addr": "staff@booklyn.org", "name": "Booklyn"},
    {"addr": "jeff@booooooom.com", "name": "Booooooom"},
    {"addr": "tips@brokelyn.com", "name": "Brokelyn work"},
    {"addr": "fernando@bax.org", "name": "Brooklyn Arts Exchange"},
    {"addr": "brooklynbybike@gmail.com", "name": "Brooklyn by Bike"},
    {"addr": "edit@brooklyneagle.net", "name": "Brooklyn Daily Eagle"},
    {"addr": "calendar@brooklyneagle.net", "name": "Brooklyn Daily Eagle"},
    {"addr": "brooklynspacesproject@gmail.com", "name": "Brooklyn Spaces"},
    {"addr": "info@brooklynstreetart.com", "name": "Brooklyn Street Art"},
    {"addr": "info@bsrlive.com", "name": "BSR (BROWN)"},
    {"addr": "musicdirector@bsrlive.com", "name": "BSR (BROWN)"},
    {"addr": "electronic@bsrlive.com", "name": "BSR (BROWN)"},
    {"addr": "lottie@breakthruradio.com", "name": "BTR"},
    {"addr": "selim.bulut@dazedmedia.com", "name": "Selim"},
    {"addr": "beyondbooking@gmail.com", "name": "Bunker"},
    {"addr": "burncrad4@gmail.com", "name": "Conrad"},
    {"addr": "katarina.hybenova@gmail.com", "name": "BushwickDaily.com"},
    {"addr": "cactusmouthblog@gmail.com", "name": "Cactus Mouth"},
    {"addr": "cliptip@gmail.com", "name": "Clip TIp"},
    {"addr": "lani@clocktower.org", "name": "Lani"},
    {"addr": "larash.zoabi@gmail.com", "name": "Coco OCD"},
    {"addr": "news@consequenceofsound.net", "name": "COS"},
    {"addr": "larry@creamcake.de", "name": "Larry"},
    {"addr": "info@curamagazine.com", "name": "Cura Magazine"},
    {"addr": "dado@ghostly.com", "name": "Dado"},
    {"addr": "jeanann.seidman@gmail.com", "name": "Jeanne"},
    {"addr": "isabella@dazedgroup.com", "name": "Isabella"},
    {"addr": "hannah@dazedgroup.com", "name": "Hannah"},
    {"addr": "dwight@secretdecoder.net", "name": "Decoder"},
    {"addr": "desertislandbrooklyn@gmail.com", "name": "Desert Island"},
    {"addr": "mail@disconaivete.com", "name": "Disco Naivite"},
    {"addr": "info@dublab.com", "name": "Dublab"},
    {"addr": "anthonywalker77@live.com", "name": "Dummy"},
    {"addr": "victoria@louisvesp.com", "name": "Victoria"},
    {"addr": "earsofthebeholder@gmail.com", "name": "Ears of the beholder"},
    {"addr": "info@eastvillageradio.com", "name": "East Village Radio"},
    {"addr": "el@diymag.com", "name": "El"},
    {"addr": "shawn@electricmustache.com", "name": "Electric Mustache"},
    {"addr": "electriczooblog@gmail.com", "name": "Electric Zoo"},
    {"addr": "info@electronicbeats.net", "name": "Electronic Beats"},
    {"addr": "slices@sensemusic.de", "name": "Electronic Beats"},
    {"addr": "emma@diymag.com", "name": "Emma"},
    {"addr": "tod@esopusmag.com", "name": "Esopus Magazine"},
    {"addr": "del@exclaim.ca", "name": "Exclaim"},
    {"addr": "webassist1@exclaim.ca", "name": "Exclaim"},
    {"addr": "james@exclaim.ca", "name": "Exclaim"},
    {"addr": "info@exoexo.xyz", "name": "exo exo gallery"},
    {"addr": "al@factmag.com", "name": "al"},
    {"addr": "promos@factmag.com", "name": "factmag"},
    {"addr": "odmyers@gmail.com", "name": "od"},
    {"addr": "duncan@thefader.com", "name": "Duncan"},
    {"addr": "ruth@thefader.com", "name": "Ruth"},
    {"addr": "info@futureaudioworkshop.com", "name": "Future Audio"},
    {"addr": "tips@flavorpill.com", "name": "Flavorpill"},
    {"addr": "humboldtrockt@googlemail.com", "name": "Lars"},
    {"addr": "georgia@fluxfactory.org", "name": "Georgia"},
    {"addr": "mail@franklinfurnace.org", "name": "Franklin Furnace"},
    {"addr": "freddie@loose-lips.co.uk", "name": "Frederic"},
    {"addr": "joshmorrissey@gmail.com", "name": "Josh"},
    {"addr": "rlanham@freewilliamsburg.com", "name": "Free Williamsburg"},
    {"addr": "mail@freewilliamsburg.com", "name": "FreeWilliamsburg.com"},
    {"addr": "nalic11@gmail.com", "name": "Friends With Both Arms"},
    {"addr": "tobiasfritzsche@yahoo.de", "name": "Tobias"},
    {"addr": "Thomas@gbh.tv", "name": "Thomas"},
    {"addr": "rotation@georgetownradio.com", "name": "Georgetown radio"},
    {"addr": "media@georgetownradio.com", "name": "Georgetown radio"},
    {"addr": "music@georgetownradio.com", "name": "Georgetown radio"},
    {"addr": "gimmetinnitus@gmail.com", "name": "Gimme Tinnitus"},
    {"addr": "glofimusic@gmail.com", "name": "Glo Fi Must Die"},
    {"addr": "chris@gorillavsbear.net", "name": "Gorilla vs. Bear"},
    {"addr": "jencarlson@gothamist.com", "name": "Gothamist"},
    {"addr": "tips@gothamist.com", "name": "Gothamist"},
    {"addr": "ubugreen@gmail.com", "name": "Mike"},
    {"addr": "greenpointers@gmail.com", "name": "Greenpointers"},
    {"addr": "info@grey-magazine.com", "name": "Grey Mag"},
    {"addr": "info@groundedtheory.de", "name": "ground theory"},
    {"addr": "contact@guerolitomusic.com", "name": "Guerolito Music"},
    {"addr": "contact@halcyonveil.com", "name": "halcyon"},
    {"addr": "redac@hartzine.com", "name": "Hart Zine"},
    {"addr": "music.headunderwater@gmail.com", "name": "Head Under Water"},
    {"addr": "thermos.unigarde@gmail.com", "name": "Kate"},
    {"addr": "tips@hyperallergic.com", "name": "Hyperallergic"},
    {"addr": "pr@hyperallergic.com", "name": "Hyperallergic"},
    {"addr": "hyperdub.records@gmail.com", "name": "hyperdub records"},
    {"addr": "us.editorial@i-d.co", "name": "I-D"},
    {"addr": "editorial@i-d.co", "name": "I-D Magazine"},
    {"addr": "editor@idiommag.com", "name": "Idiom", "error": True},
    {"addr": "StuffHipstersHate@gmail.com", "name": "Stuff Hipsters Hate"},
    {"addr": "hq@brooklynrail.org", "name": "The Brooklyn Rail"},
    {"addr": "scoop@huffingtonpost.com", "name": "The Huffington Post"},
    {"addr": "DW@theworldsbestever.com", "name": "The World's Best Ever"},
    {"addr": "libbyrosof@gmail.com", "name": "THEartblog.org"},
    {"addr": "queries@wagmag.org", "name": "Wagmag"},
    {"addr": "partyhotline@thecobrasnake.com", "name": "The Cobrasnake"},
    {"addr": "paolo@thedelimagazine.com", "name": "Paolo"},
    {"addr": "editor@tinymixtapes.com", "name": "Tiny Mix Tapes"},
    {"addr": "jay@tinymixtapes.com", "name": "Jay"},
    {"addr": "AlexLaurence@AOL.com", "name": "The Portable Infinite", "error": True},
    {"addr": "ME@YOURSTRU.LY", "name": "Yours Truly", "error": True},
    {"addr": "noah@thefmly.com", "name": "The Fmly"},
    {"addr": "info@thefmly.com", "name": "The Fmly"},
    {"addr": "hello@smokedontsmoke.com", "name": "Smoke Don’t SMoke"},
    {"addr": "ohunholyrhythms@gmail.com", "name": "Unholy Rhythms"},
    {"addr": "music@sunsetintherearview.com", "name": "Sunset In The Rearview"},
    {"addr": "styrofoamdrone@gmail.com", "name": "Styrofoam Drone"},
    {"addr": "yvynyl@gmail.com", "name": "Yvynyl"},
    {"addr": "sean@saidthegramophone.com", "name": "Said The Gramophone"},
    {"addr": "jordan@saidthegramophone.com", "name": "Said The Gramophone"},
    {"addr": "dan@saidthegramophone.com", "name": "Said The Gramophone"},
    {"addr": "alex.miller@vice.com", "name": "Vice"},
    {"addr": "editor@vice.com", "name": "Vice"},
    {"addr": "derek@imposemagazine.com", "name": "Impose"},
    {"addr": "info@imposemagazine.com", "name": "Impose"},
    {"addr": "thesoundtheymake@gmail.com", "name": "The Sound They Make"},
    {"addr": "corneliusquabeck@hotmail.com", "name": "INFINITE GREYSCALE RECORDS"},
    {"addr": "intervieweditor@brantpub.com", "name": "Interview MAg"},
    {"addr": "info@intothewoods.tv", "name": "Into the Woods"},
    {"addr": "hello@itsnicethat.com", "name": "It’s Nice That"},
    {"addr": "submit@itsnicethat.com", "name": "It’s Nice That"},
    {"addr": "ls@itsnicethat.com", "name": "It’s Nice That"},
    {"addr": "editorial@xlr8r.com", "name": "XLR8R"},
    {"addr": "submissions@wineandbowties.com", "name": "Wine and Bowties"},
    {"addr": "jamie@diymag.com", "name": "Jamie"},
    {"addr": "jmandelbooking@gmail.com", "name": "Jason"},
    {"addr": "jens@jens-friebe.de", "name": "Jens"},
    {"addr": "info@spektacle.com", "name": "John"},
    {"addr": "submissions@juxtapoz.com", "name": "Juxtapoz"},
    {"addr": "videostatic@gmail.com", "name": "Video Static"},
    {"addr": "newmusic@wkcr.org", "name": "WKCR (Columbia University)"},
    {"addr": "pd@wknc.org", "name": "WKNC (University of NC)"},
    {"addr": "underground@wknc.org", "name": "WKNC (University of NC)"},
    {"addr": "music@wnyu.org", "name": "WNYU (NYU)"},
    {"addr": "maketheproduct@wnyu.org", "name": "WNYU (NYU)"},
    {"addr": "wbar@barnard.edu", "name": "WBAR (Barnard College)"},
    {"addr": "mail@whrb.org", "name": "WHRB (Harvard)"},
    {"addr": "pd@whrb.org", "name": "WHRB (Harvard)"},
    {"addr": "music@wybc.com", "name": "WYBC (Yale)"},
    {"addr": "contact@whpk.org", "name": "WHPK (Chicago University)"},
    {"addr": "md@whpk.org", "name": "WHPK (Chicago University)"},
    {"addr": "music@wxdu.org", "name": "WXDU (Duke)"},
    {"addr": "rpm@wxdu.org", "name": "WXDU (Duke)"},
    {
        "addr": "eirahe@saic.edu",
        "name": "The School of the Art Institute of Chicago Radio",
    },
    {"addr": "a.ascari@kaleidoscope-press.com", "name": "Kaledoscope", "error": True},
    {"addr": "b.stoppani@kaleidoscope-press.com", "name": "Kaledoscope", "error": True},
    {"addr": "mschlutt@kaltblut-magazine.com", "name": "Marcel"},
    {"addr": "music@kalx.berkeley.edu", "name": "KALX (Berkley)"},
    {"addr": "mail@kcrw.org", "name": "KCRW"},
    {"addr": "md@kfjc.org", "name": "KFJC"},
    {"addr": "jlazar6987@gmail.com", "name": "KFJC"},
    {"addr": "soil@kfjc.org", "name": "KFJC"},
    {"addr": "rpm@wbor.org", "name": "WBOR"},
    {"addr": "newedge@wmbr.org", "name": "WMBR (MIT)"},
    {"addr": "music@wmbr.org", "name": "WMBR (MIT)"},
    {"addr": "md@wxyc.org", "name": "WXYC"},
    {"addr": "whrbrock@gmail.com", "name": "WHRB ROCK"},
    {"addr": "fffreakout@hotmail.com", "name": "KMSU Freeform Freakout"},
    {"addr": "knowphase@gmail.com", "name": "Know Phase"},
    {"addr": "knoxroadblog@gmail.com", "name": "Knox Road"},
    {"addr": "justus.koehnke@kaput-mag.com", "name": "Justus"},
    {"addr": "info@kutx.org", "name": "KUTX"},
    {"addr": "info@theWGnews.com", "name": "Williamsburg Greenpoint News+Arts"},
    {"addr": "art@timeoutny.com", "name": "TimeOut New York"},
    {"addr": "SPark@CNGLocal.com", "name": "The Brooklyn Paper"},
    {"addr": "jeff@greenpointnews.com", "name": "The Greenpoint Gazette"},
    {"addr": "art@brooklynrail.org", "name": "The Brooklyn Rail"},
    {"addr": "showpaper@gmail.com", "name": "Showpaper"},
    {"addr": "info@tacet.eu", "name": "Tacet"},
    {"addr": "madeline@sentimentalistmag.com", "name": "Sentimentalist"},
    {"addr": "zipco@superchief.tv", "name": "Superchief"},
    {"addr": "bill@superchief.tv", "name": "Brandon"},
    {"addr": "submissions@superchief.tv", "name": "Superchief"},
    {"addr": "editor@superchief.tv", "name": "Superchief"},
    {"addr": "info@thelast-magazine.com", "name": "The Last Magazine"},
    {"addr": "musiclistings.ny@timeout.com", "name": "Time Out"},
    {"addr": "culture@theguardian.com", "name": "The Guardian"},
    {"addr": "contact@vagamagazine.com", "name": "Vaga"},
    {"addr": "aparks@self-titledmag.com", "name": "Andrew"},
    {"addr": "adworken@self-titledmag.com", "name": "Self-Titled Mag"},
    {"addr": "music@kzsu.stanford.edu", "name": "KZSU (STANFORD)"},
    {"addr": "fortherecord@larecord.com", "name": "LA Record"},
    {"addr": "danc@larecord.com", "name": "LA Record"},
    {"addr": "info@sexmagazine.us", "name": "Sex Mag"},
    {"addr": "david@thetwopercent.com", "name": "David"},
    {"addr": "toddp@toddpnyc.com", "name": "Todd"},
    {"addr": "skeletons@shinkoyo.com", "name": "Shinkoyo"},
    {"addr": "transculturepartysystem@gmail.com", "name": "Trans-Culture Party System"},
    {"addr": "noradab@gmail.com", "name": "Nora"},
    {"addr": "Joseph.S.Ahearn@gmail.com", "name": "Joseph"},
    {"addr": "jordanmichael39@gmail.com", "name": "Jordan"},
    {"addr": "meronkun@gmail.com", "name": "Babycastles"},
    {"addr": "nathaniel.roe@gmail.com", "name": "Nathaniel"},
    {"addr": "info@ladygunn.com", "name": "Ladygunn"},
    {"addr": "info@lanewayfestival.com.au", "name": "Lane"},
    {"addr": "mariolasar@aol.com", "name": "Mario"},
    {"addr": "info@le-dojo.org", "name": "Le Dojo"},
    {"addr": "doncablin@gmail.com", "name": "Yuchen"},
    {"addr": "lisa@diymag.com", "name": "Lisa"},
    {"addr": "looselipsfreddie@gmail.com", "name": "Freddie"},
    {"addr": "mg@lurvemag.com", "name": "Lurve Mag"},
    {"addr": "champagnesequins@gmail.com", "name": "Wes"},
    {"addr": "partycipate@torstrassenfestival.de", "name": "Melissa Perales"},
    {"addr": "jamesamoeba@gmail.com", "name": "James"},
    {"addr": "metal@revistametal.com", "name": "Metal Magazine"},
    {"addr": "funkverteiler@gmx.de", "name": "Michel"},
    {"addr": "phillip.sollmann@kaput-mag.com", "name": "Phillip"},
    {"addr": "john.stanier@kaput-mag.com", "name": "John"},
    {"addr": "sarahszczesny@hotmail.com", "name": "Sarah"},
    {"addr": "max.freudenschuss@kaput-mag.com", "name": "Max"},
    {"addr": "anton@mansionsandmillions.com", "name": "Armand"},
    {"addr": "sarah@popmontreal.com", "name": "Sarah"},
    {"addr": "s.ingenhoff@gmx.de", "name": "Sebastian"},
    {"addr": "saskika.timm@kaput-mag.com", "name": "Saskia"},
    {"addr": "bootlegtapesbk@gmail.com", "name": "Jon"},
    {"addr": "phil@subbacultcha.nl", "name": "Phil"},
    {"addr": "maija@subbacultcha.nl", "name": "Maija"},
    {"addr": "nathan@sameheads.com", "name": "Nathan"},
    {"addr": "kevinmhalpin@gmail.com", "name": "Kevin"},
    {"addr": "contact@democrazy.be", "name": "Democrazy"},
    {"addr": "brian@vibesinternational.com", "name": "Brian", "error": True},
    {"addr": "press@xlr8r.com", "name": "xlr8r"},
    {"addr": "miojoindie@gmail.com", "name": "Miojo Indie"},
    {"addr": "info@mixedmanagement.com", "name": "mixed mgmt"},
    {"addr": "seb@mixmagmedia.com", "name": "Seb"},
    {"addr": "sean@mixmagmedia.com", "name": "Sean"},
    {"addr": "anna.zanes@officemagazine.net", "name": "Anton"},
    {"addr": "lazuritelazurite@gmail.com", "name": "Megan"},
    {"addr": "monique@monsterchildren.com", "name": "Monique"},
    {"addr": "info@moussemagazine.it", "name": "Mousse mag"},
    {"addr": "redazione@musemagazine.it", "name": "Muse Mag"},
    {"addr": "mark@music.for-robots.com", "name": "Mark"},
    {"addr": "info@mutek.org", "name": "mutek"},
    {"addr": "bcn@mutek.org", "name": "Mutek"},
    {"addr": "argentina@mutek.org", "name": "mutek"},
    {"addr": "mvcarbon@gmail.com", "name": "Mv Carbon"},
    {"addr": "lucalopinto@neromagazine.it", "name": "Nero Mag"},
    {"addr": "valeriomannucci@neromagazine.it", "name": "Nero Mag"},
    {"addr": "info@newarttv.com", "name": "New Art TV"},
    {"addr": "travis@newdust.com", "name": "New Dust"},
    {"addr": "josh@newdust.com", "name": "New Dust"},
    {"addr": "quentin.newdust@yahoo.com", "name": "New Dust"},
    {"addr": "artlistings@nymag.com", "name": "New York Magazine"},
    {"addr": "tips@observer.com", "name": "New York Observer"},
    {"addr": "missheather@thatgreenpointblog.com", "name": "New York Shitty"},
    {"addr": "art@newmanfestival.com", "name": "newman festival"},
    {"addr": "contact@newtownradio.com", "name": "Newtown Radio"},
    {"addr": "info@nkprojekt.de", "name": "ni projekt"},
    {"addr": "night@nightmag.com", "name": "Night Mag"},
    {"addr": "submissions@nofearofpop.net", "name": "No Fear Of Pop"},
    {"addr": "henning@nofearofpop.net", "name": "No Fear Of Pop"},
    {"addr": "parker@nofearofpop.net", "name": "No Fear Of Pop"},
    {"addr": "theregattasgroup@gmail.com", "name": "Northern Spy"},
    {"addr": "adam@northern-spy.com", "name": "Northern Spy"},
    {"addr": "promos@ntslive.co.uk", "name": "nts"},
    {"addr": "hello@ntslive.co.uk", "name": "nts"},
    {"addr": "redaction@numero-magazine.com", "name": "Numero mag"},
    {"addr": "event@nyartbeat.com", "name": "NY Art Beat"},
    {"addr": "editorialsubmissions@nymag.com", "name": "NY mag"},
    {"addr": "mrossier@nyfa.org", "name": "NYFA"},
    {"addr": "nyoffice@strausnews.com", "name": "NYPRESS"},
    {"addr": "offtheradargroup@gmail.com", "name": "Off The Radar"},
    {"addr": "offtheradarmiami@gmail.com", "name": "Off The Radar"},
    {"addr": "erikaisit@gmail.com", "name": "Off The Radar"},
    {"addr": "dalya@officemagazine.net", "name": "Dalya"},
    {"addr": "ivan@officemagazine.net", "name": "Ivan"},
    {"addr": "michael@stereogum.com", "name": "Michael"},
    {"addr": "tom@stereogum.com", "name": "Tom"},
    {"addr": "chris@stereogum.com", "name": "Chris"},
    {"addr": "features@tinymixtapes.com", "name": "Tiny Mix Tapes"},
    {"addr": "chocolategrinder@tinymixtapes.com", "name": "Tiny Mix Tapes"},
    {"addr": "news@tinymixtapes.com", "name": "Tiny Mix Tapes"},
    {"addr": "opbmusic@opb.org", "name": "OPB"},
    {"addr": "lesyeuxorange@gmail.com", "name": "Les Yeux Orange"},
    {"addr": "web@oystermag.com", "name": "Oyster"},
    {"addr": "editorial@oystermag.com", "name": "Oyster Magazine"},
    {"addr": "staff@splendourinthegrass.com", "name": "Splendour"},
    {"addr": "edit@papermag.com", "name": "Paper"},
    {"addr": "INFO@PASUNAUTRE.COM", "name": "Pas Un Autre"},
    {"addr": "bonnie@pastemagazine.com", "name": "Paste Mag"},
    {"addr": "tyler@pastemagazine.com", "name": "Paste Mag"},
    {"addr": "dacey@pastemagazine.com", "name": "Paste Mag"},
    {"addr": "qmail@qthemusic.com", "name": "Paul"},
    {"addr": "sarah@diymag.com", "name": "Sarah"},
    {"addr": "will@diymag.com", "name": "Will"},
    {"addr": "info@sasquatchfestival.com", "name": "Sasquatch Festival"},
    {"addr": "info@sunsetevents.com.au", "name": "Sunset Festival"},
    {"addr": "enquiries@nextmedia.com.au", "name": "Yen Magazine"},
    {"addr": "MAIL@TRISTANPERICH.COM", "name": "Tristan"},
    {"addr": "periscope.lyon@gmail.com", "name": "Periscope Lyon"},
    {"addr": "phillips@pitchfork.com", "name": "Phillip"},
    {"addr": "news@pitchfork.com", "name": "Pitchfork"},
    {"addr": "rj@pitchfork.com", "name": "RJ"},
    {"addr": "larry@pitchfork.com", "name": "Larry"},
    {"addr": "markr@pitchfork.com", "name": "Mark"},
    {"addr": "jenn@pitchfork.com", "name": "Jenn"},
    {"addr": "editor@portable.tv", "name": "Portable"},
    {"addr": "info@portable.tv", "name": "Portable"},
    {"addr": "ryan@portalsmusic.com", "name": "Portals"},
    {"addr": "jake@portalsmusic.com", "name": "Portals"},
    {"addr": "eloise@portalsmusic.com", "name": "Portals"},
    {"addr": "nina@portalsmusic.com", "name": "Portals"},
    {"addr": "PrattRadio@gmail.com", "name": "PRATT radio"},
    {"addr": "info@princecharlesberlin.com", "name": "Prince Charles"},
    {"addr": "jjenkin@printedmatter.org", "name": "Printed Matter"},
    {"addr": "david@promonews.tv", "name": "Promo News"},
    {"addr": "inquire@purple.fr", "name": "Purple Mag"},
    {"addr": "rain@purple.fr", "name": "Purple Mag"},
    {"addr": "qca@queenscouncilarts.org", "name": "Queens Council on the Arts"},
    {"addr": "dj@radio1190.org", "name": "Radio 1190"},
    {"addr": "sam@radio1190.org", "name": "Radio 1190"},
    {"addr": "carmen@radio1190.org", "name": "Radio 1190"},
    {"addr": "leskin@radio1190.org", "name": "Radio 1190"},
    {"addr": "gabe@futureforever.net", "name": "Gabe"},
    {"addr": "contact@revue-et-corrigee.net", "name": "Revue Et Corrigée"},
    {"addr": "contact@revuevolume.fr", "name": "Revue Volume"},
    {"addr": "contact@fubiz.net", "name": "Romain"},
    {"addr": "demos@roughtraderecords.com", "name": "Rough Trade"},
    {"addr": "INFO@THEWILD.COM", "name": "The Wild Mag"},
    {"addr": "connection@superior-magazine.com", "name": "Superior Mag"},
    {"addr": "info@sameheads.com", "name": "sameheads"},
    {"addr": "team@torstrassenfestival.de", "name": "Torstrassen Fest"},
    {"addr": "info@visionsfestival.com", "name": "Visions Fest"},
    {"addr": "info@vmagazine.com", "name": "vmagazine"},
    {"addr": "info@signalsfromtheperiphery.ee", "name": "Signal from the Periphery"},
    {"addr": "nicolas@warprecords.com", "name": "Nicolas"},
    {"addr": "promokraviz@gmail.com", "name": "trip"},
    {"addr": "tom@planet.mu", "name": "Tom"},
    {"addr": "promo@truantsblog.com", "name": "Truants"},
    {"addr": "soraya@truantsblog.com", "name": "Soraya"},
    {"addr": "jess@truantsblog.com", "name": "Jess"},
    {"addr": "mute@mute.com", "name": "Mute"},
    {"addr": "blowinguptheworkshop@gmail.com", "name": "Blowing Up The Workshop"},
    {"addr": "clubbalaclub@gmail.com"},
    {"addr": "info@whiti.es", "name": "WHITIES"},
    {"addr": "j@julianschoen.com", "name": "Julian"},
    {"addr": "Supertuffrecords@gmail.com", "name": "Michael"},
    {"addr": "contact@charstiles.com", "name": "Char"},
    {"addr": "booking@tombolo.live", "name": "Miguel"},
    {"addr": "Thelevelparty@gmail.com"},
    {"addr": "frenchpresslounge@gmail.com", "name": "Kyle  Lyon"},
    {"addr": "honesocialny@gmail.com", "name": "Masha"},
    {"addr": "teamdhgbookings@gmail.com"},
    {"addr": "evan@popgunpresents.com", "name": "Evan"},
    {"addr": "clarkprice1986@gmail.com", "name": "Clark"},
    {"addr": "seinfeldthedj@gmail.com", "name": "Benjamin"},
    {"addr": "pipipigroup88@gmail.com", "name": "pipipi"},
    {"addr": "saynothinglabel@gmail.com", "name": "Say Nothing"},
    {"addr": "abdes953@newschool.edu", "name": "Selwa"},
    {"addr": "Micah@future-everything.com", "name": "Future-Everything"},
    {"addr": "Seth@solarplexia.com", "name": "Setha"},
    {"addr": "Vadabooking@gmail.com"},
    {"addr": "brandon@igetrvng.com", "name": "Brandon"},
    {"addr": "phil@igetrvng.com", "name": "Phil"},
    {"addr": "paulraffaele@gmail.com", "name": "Paul"},
    {"addr": "tomekgunzel@gmail.com", "name": "Tomek"},
    {"addr": "ronanscooby@gmail.com", "name": "Eternal Ocean"},
    {"addr": "emissivebookings@gmail.com"},
    {"addr": "brandon@residentadvisor.net", "name": "Brandon"},
    {"addr": "riccardo@truantsblog.com", "name": "Riccardo"},
    {"addr": "Radio@motellacast.live"},
    {"addr": "mail@raulzahir.com", "name": "All Our Noise"},
    {"addr": "malikmusic_2000@yahoo.com", "name": "Malik"},
    {"addr": "juddhower@gmail.com", "name": "Crescent Booking"},
    {"addr": "edanwilber@gmail.com", "name": "Dead By Audio"},
    {"addr": "ddiilliiaann@gmail.com", "name": "Dilian"},
    {"addr": "patricfallon@gmail.com", "name": "Patric"},
    {"addr": "wyattdstevens@gmail.com", "name": "Wyatt"},
    {"addr": "win.e.coop.r@gmail.com", "name": "Winnie"},
    {"addr": "alex@alexlemieux.cc", "name": "Alex"},
    {"addr": "printed@lqqkstudio.com", "name": "LQQK"},
    {"addr": "peter.181@gmail.com", "name": "Peter"},
    {"addr": "aarontrippy@gmail.com", "name": "Aaronn"},
    {"addr": "ask4mikesheffield@gmail.com", "name": "Mike"},
    {"addr": "Randolph.riback@gmail.com", "name": "Randy  Riback"},
    {"addr": "Heidisabertooth@gmail.com", "name": "Heidi  Sabers"},
    {"addr": "support@vfiles.com", "name": "v files"},
    {"addr": "i@toitoimusik.com", "name": "toi toi"},
    {"addr": "o.oroma@gmail.com", "name": "Olga"},
    {"addr": "olivetonic@gmail.com", "name": "Crystal"},
    {"addr": "douglasmcconachie@mail.com", "name": "Douglas"},
    {"addr": "ibjohndoe@gmail.com"},
    {"addr": "keoni.bedania@gmail.com"},
    {"addr": "cayofelipesdj@gmail.com"},
    {"addr": "imluishock@gmail.com"},
    {"addr": "dj@stoneypie.com"},
    {"addr": "Numbertheory666@gmail.com"},
    {"addr": "Georgegates456@gmail.com", "name": "George"},
    {"addr": "djcesmusic@gmail.com"},
    {"addr": "megwardmusic@gmail.com"},
    {"addr": "zefgirlclubmusic@gmail.com"},
    {"addr": "ljleonar@icloud.com"},
    {"addr": "yellowislandrecords@gmail.com", "name": "Yellow Island Records"},
    {"addr": "hcbaitz@gmail.com"},
    {"addr": "djaudio1music@gmail.com"},
    {"addr": "inka.sublow@gmail.com"},
    {"addr": "Grytnessd@gmail.com"},
    {"addr": "RockMyParty@djallthewaykay.com"},
    {"addr": "arenamovedizo@gmail.com"},
    {"addr": "leo.gesell@gmx.de", "name": "Leo"},
    {"addr": "dhunicutt@gmail.com"},
    {"addr": "lookylooky@gmail.com"},
    {"addr": "habojames@gmail.com"},
    {"addr": "nadiakhanmusic@gmail.com"},
    {"addr": "info@vvnout.com"},
    {"addr": "paythedj@suchordie.com"},
    {"addr": "chrisulloa@yahoo.com", "name": "Greg"},
    {"addr": "Djwawabk@gmail.com", "name": "Greg"},
    {"addr": "ssylvesteralva@gmail.com"},
    {"addr": "jt@clubspace.com"},
    {"addr": "cole.evelev@gmail.com", "name": "Cole"},
    {"addr": "helder.m.menor@gmail.com"},
    {"addr": "stmsammcd@gmail.com"},
    {"addr": "jules@jules.nyc", "name": "Jules"},
    {"addr": "mirafahrenheit@gmail.com"},
    {"addr": "contactingkyle@gmail.com"},
    {"addr": "getkkaned@hotmail.com"},
    {"addr": "Worthandzell@gmail.com"},
    {"addr": "mustar.convolve@gmail.com"},
    {"addr": "keithcushner@gmail.com", "name": "Keith"},
    {"addr": "hello@lenorajayne.com", "name": "Lenora"},
    {"addr": "Salleymane@gmail.com"},
    {"addr": "Sblkringer@gmail.com"},
    {"addr": "alec.sugar@gmail.com", "name": "Alexander"},
    {"addr": "mario.loncarek@gmail.com"},
    {"addr": "Info@nainaldn.com"},
    {"addr": "greenhausradio@gmail.com"},
    {"addr": "djkaseyriot@gmail.com"},
    {"addr": "Djgcue504@gmail.com"},
    {"addr": "mike.asher@hotmail.com", "name": "Mike"},
    {"addr": "yuknodis@gmail.com"},
    {"addr": "Djbowlcutkr@gmail.com", "name": "You"},
    {"addr": "poolboimgmt@gmail.com"},
    {"addr": "d3ltat3a@gmail.com"},
    {"addr": "Georgethomasjames@hotmail.com", "name": "George"},
    {"addr": "Tunes4cotton@gmail.com"},
    {"addr": "innovatemiami305@gmail.com"},
    {"addr": "nikhil.sonnad@gmail.com", "name": "Nikhil"},
    {"addr": "jamie@jameswalford.com", "name": "Jamie"},
    {"addr": "misfitus@gmail.com"},
    {"addr": "info@glacci.net"},
    {"addr": "a@avalonemerson.com", "name": "Avalon"},
    {"addr": "rclovermusic@gmail.com", "name": "Ryan"},
    {"addr": "stricty@strictface.net"},
    {"addr": "michael.s.baltra@gmail.com", "name": "Michael"},
    {"addr": "safwanelzubair94@gmail.com"},
    {"addr": "Johzee.bristol@gmail.com"},
    {"addr": "dam_pits@hotmail.com"},
    {"addr": "info@magongomusic.com"},
    {"addr": "stacey.sexton.official@gmail.com"},
    {"addr": "annarizkalla@gmail.com"},
    {"addr": "tadeomusic@gmail.com"},
    {"addr": "gardnauk@live.co.uk"},
    {"addr": "bainkg@gmail.com", "name": "Kyle"},
    {"addr": "hello@isaacirwin.com", "name": "Isaac"},
    {"addr": "Callumsoldan@yahoo.co.uk"},
    {"addr": "healanick@gmail.com", "name": "Lani"},
    {"addr": "info@joseyrebelle.com"},
    {"addr": "Djmaryp@gmail.com"},
    {"addr": "keanuorange@keanuorange.com"},
    {"addr": "barkingsounds@gmail.com"},
    {"addr": "Somegirldj@gmail.com"},
    {"addr": "tunes4raheim@gmail.com"},
    {"addr": "caolan.mcgagh@gmail.com"},
    {"addr": "djwhitenite@gmail.com"},
    {"addr": "vabryandodson@gmail.com"},
    {"addr": "derekrusso@gmail.com", "name": "Derek"},
    {"addr": "bawsamgmt@gmail.com"},
    {"addr": "hubert.grabusinski@gmail.com", "name": "Hubert"},
    {"addr": "bondr@tcd.ie"},
    {"addr": "antoine.abikhalil@ehl.ch", "name": "Antoine"},
    {"addr": "jaymonds@mediaworks.co.nz"},
    {"addr": "fa98music@gmail.com"},
    {"addr": "bigsip6993@hotmail.de"},
    {"addr": "Incorporatecompany@gmail.com", "name": "Fernelly"},
    {"addr": "Benjiolloyd@hotmail.com", "name": "Benji"},
    {"addr": "Musicforsunsets@gmail.com"},
    {"addr": "peter@thehogans.com"},
    {"addr": "mimimelaninmusic@gmail.com"},
    {"addr": "concretejungyals@gmail.com"},
    {"addr": "trayzepromos@gmail.com"},
    {"addr": "get@ajaxx.co"},
    {"addr": "djtaye@gmail.com"},
    {"addr": "djbobby909@gmail.com"},
    {"addr": "wielfriedf@gmail.com"},
    {"addr": "Rich@seoulcommunityradio.com", "name": "Rich"},
    {"addr": "trimpage1234@yahoo.com"},
    {"addr": "tltrostle@gmail.com"},
    {"addr": "info@lobstertheremin.com", "name": "Lobster Theremin"},
    {"addr": "Badgemanfox@hotmail.com"},
    {"addr": "contact.theskeletonking@gmail.com"},
    {"addr": "Abbipress@gmail.com", "name": "Abbi  Press"},
    {"addr": "logan.williams8325@gmail.com", "name": "Logan"},
    {"addr": "aleeve.music@gmail.com"},
    {"addr": "tina.kittelty@xtra.co.nz"},
    {"addr": "brian@gltd.email", "name": "Brian  Abelson"},
    {"addr": "mphatiq@gmail.com"},
    {"addr": "djbcause@gmail.com"},
    {"addr": "hallpatrickjames@yahoo.com", "name": "Patrick"},
    {"addr": "bombjahlaam@gmail.com"},
    {"addr": "baldmankid@gmail.com"},
    {"addr": "harveybradshaw@sky.com", "name": "Harvey"},
    {"addr": "Pocketshufflemusic@gmail.com"},
    {"addr": "donnchahanks@outlook.com"},
    {"addr": "djdommb@gmail.com"},
    {"addr": "info.daskapital@gmail.com", "name": "Das Kaptial"},
    {"addr": "bnkzzz@gmail.com"},
    {"addr": "caitbfahey@gmail.com", "name": "Cait"},
    {"addr": "Info@djvectra.co.uk"},
    {"addr": "thisiscriscomusic@gmail.com"},
    {"addr": "rustylunn@hotmail.com", "name": "Rusty"},
    {"addr": "robseurat@gmail.com", "name": "Rob"},
    {"addr": "khadija.bhuiyan@gmail.com", "name": "Khadija"},
    {"addr": "tobiasbkahn@gmail.com", "name": "Toby"},
    {"addr": "mollymhyland@gmail.com", "name": "Molly"},
    {"addr": "1djktm@gmail.com"},
    {"addr": "ryulee@hotmail.com"},
    {"addr": "wdavies1234@googlemail.com"},
    {"addr": "tahchecksemails@gmail.com", "name": "Tah"},
    {"addr": "Djkmor88@gmail.com"},
    {"addr": "christiane@xtendedrelease.org", "name": "Christiane"},
    {"addr": "sarahzmamo@gmail.com", "name": "Sarah"},
    {"addr": "taghzouty@gmail.com"},
    {"addr": "Pictureplanes@gmail.com"},
    {"addr": "dgrabliauskas@gmail.com"},
    {"addr": "bakegla@gmail.com"},
    {"addr": "floraflightmod@gmail.com"},
    {"addr": "cnewmarch@gmail.com"},
    {"addr": "Bookprincehakeem@gmail.com"},
    {"addr": "djyungsingh@outlook.com"},
    {"addr": "craigrhandfield@gmail.com", "name": "Craig"},
    {"addr": "russellferguson1@outlook.com", "name": "Russell"},
    {"addr": "will.colgate4@googlemail.com", "name": "Will"},
    {"addr": "bookcleotrvppv@gmail.com"},
    {"addr": "flynn.leander@gmail.com"},
    {"addr": "robbiemarksmusic@gmail.com"},
    {"addr": "simonas.cinga@gmail.com"},
    {"addr": "johnson01@hotmail.co.uk"},
    {"addr": "rosy@lobstertheremin.com", "name": "Rosy"},
    {"addr": "ausschussausschuss@gmail.com"},
    {"addr": "adamthechannel@gmail.com"},
    {"addr": "Djhazeusmusic@gmail.com"},
    {"addr": "Alanboomer@gmail.com"},
    {"addr": "Snadbad@gmail.com"},
    {"addr": "get@djplaturn.com"},
    {"addr": "teocerni95@gmail.com"},
    {"addr": "juliendecastilho@gmail.com"},
    {"addr": "Rorysmith1998@gmail.com"},
    {"addr": "Info@catherineouellette.com"},
    {"addr": "awooferdj@gmail.com"},
    {"addr": "hello.milkyshake@gmail.com"},
    {"addr": "Cindy@itsnotuits.me", "name": "Cindy"},
    {"addr": "Codyaspeneichelberger@gmail.com"},
    {"addr": "Gone.projects@gmail.com"},
    {"addr": "noanunoparty@gmail.com"},
    {"addr": "Rosegawd@gmail.com"},
    {"addr": "aaron.rutherford95@gmail.com", "name": "Aaron"},
    {"addr": "Julienneby@gmail.com"},
    {"addr": "hoomar270496@gmail.com"},
    {"addr": "Project_37@outlook.com"},
    {"addr": "eddie.stats.houghton@gmail.com", "name": "Eddie"},
    {"addr": "music@bassbearmusic.net"},
    {"addr": "Bradley.west@outlook.com", "name": "Bradley"},
    {"addr": "vicki.siolos@gmail.com", "name": "Vicki"},
    {"addr": "info@kaabaemoji.biz"},
    {"addr": "Pinky4prez@gmail.com"},
    {"addr": "teodor.meurling@gmail.com"},
    {"addr": 'zunigajamel@gmail.com"'},
    {"addr": "eauxzown@gmail.com"},
    {"addr": "rodrigogares@hotmail.com", "name": "Rodrigo"},
    {"addr": "uday_tomar@hotmail.com", "name": "Uday"},
    {"addr": "Powertoolsmixshow@gmail.com"},
    {"addr": "thejakejellis@gmail.com", "name": "Jake"},
    {"addr": "bobbyhorton@gmail.com", "name": "Bobby"},
    {"addr": "Antonradkemusic@gmail.com", "name": "Anton"},
    {"addr": "Horn1890@gmail.com"},
    {"addr": "george99iou@gmail.com", "name": "George"},
    {"addr": "info@missingtextures.com"},
    {"addr": "Breniecia@gmail.com"},
    {"addr": "qutaq@aol.com"},
    {"addr": "exilegion@gmail.com", "name": "Rob"},
    {"addr": "Olliejamo@outlook.com"},
    {"addr": "contact@shyeyez.com"},
    {"addr": "patientfrei@gmail.com", "name": "Jon"},
    {"addr": "hr6jakebob@hotmail.co.uk"},
    {"addr": "Kushjonesy@gmail.com", "name": "Kush"},
    {"addr": "sippinlemonfanta@gmail.com"},
    {"addr": "Laurenceguymusic@gmail.com", "name": "Laurence"},
    {"addr": "Level10ent@sbcglobal.net"},
    {"addr": "Idomalka173@gmail.com"},
    {"addr": "ineffektnl@gmail.com"},
    {"addr": "contact@jayemkayem.com"},
    {"addr": "Lockedgrooves@gmail.com"},
    {"addr": "Bluedollarbillz@gmail.com"},
    {"addr": "Damagedgoodsco@icloud.com"},
    {"addr": "franceselyseallan@gmail.com"},
    {"addr": "monaefreeman8@gmail.com", "name": "Monae"},
    {"addr": "lilcheeks@hotmail.com"},
    {"addr": "Silvaofficialmusic@gmail.com"},
    {"addr": "info@jadalareign.com"},
    {"addr": "taniyee@gmail.com", "name": "Tani"},
    {"addr": "tkbchamp@gmail.com"},
    {"addr": "Jabairk@gmail.com"},
    {"addr": "Andrearostron@gmail.com", "name": "Andrea"},
    {"addr": "yungmusicalmatcha@gmail.com"},
    {"addr": "rebeccatgk@gmail.com", "name": "Becca"},
    {"addr": "lukethorne@outlook.com"},
    {"addr": "flmwrk@gmail.com"},
    {"addr": "Oe.presents@gmail.com"},
    {"addr": "timothy.d97@gmail.com", "name": "Timothy"},
    {"addr": "muzzybearr@gmail.com"},
    {"addr": "justin@eternaleditions.com", "name": "Justin"},
    {"addr": "Jacob.cusumano@gmail.com", "name": "Jacob"},
    {"addr": "jgeffroy69@gmail.com"},
    {"addr": "Selectronicbc1@gmail.com"},
    {"addr": "itslisaluo@gmail.com", "name": "Lisa"},
    {"addr": "tomcolorline@gmail.com", "name": "Tom"},
]

SENT_TO_EMAILS = frozenset([e["addr"] for e in SENT_TO])


class Dispatch(object):
    """
    A class for representing a dispatch markdown file and its contents.
    """

    CAT_URL = "gltd.cat"

    # regexes
    RE_PATH = re.compile(
        r"^.+/([a-z\-]+)/([0-9]{4})\-([0-9]{2})\-([0-9]{2})_[a-z]+\-[a-z]+\-([0-9]+)_(.+).md$"
    )

    # asset cache
    space = platform.DOAssets()
    assets = {}

    def __init__(self, path):
        """
        :param path: The path to the markdown template for the dispatch, with jinja logic.
        """
        self.path = path

    # md helpers

    def _verify_path(self, path):
        """
        Verify a path
        """
        try:
            assert os.path.exists(path)
        except AssertionError:
            raise exc.ConfigError(f"Dispatch path {path} does not exit!")

    def _parse_path(self, path):
        """
        Parse path
        """
        if not path.startswith("/"):
            path = os.path.join(DISPATCHES_DIR, path)
        self._verify_path(path)
        m = self.RE_PATH.search(path)
        if not m:
            raise exc.ConfigError(f"Invalid md path: {path}")
        return dict(
            path=path,
            type=m.group(1),
            year=int(m.group(2)),
            month=int(m.group(3)),
            day=int(m.group(4)),
            release_num=int(m.group(5)),
            cat_num=f"{self.CAT_URL}/{m.group(5)}",
            slug=m.group(6).strip(),
        )

    def _load_path_data(self, path):
        """
        Parse contents of markdown file
        """
        path_data = self._parse_path(path)
        raw = open(path_data["path"]).read()
        raw = raw.split("<!-----")[-1]
        path_data["_meta_tmpl"] = Template(raw.split("---->")[0].strip())
        path_data["_content_tmpl"] = Template(raw.split("---->")[-1].strip())
        return path_data

    def upload_assets(self, assets):
        """
        Upload assets to do spaces, caching by path.
        """
        for name, asset in assets.items():
            if name not in self.assets:
                # upload the asset to digital ocean
                local_path = os.path.join(ASSETS_DIR, asset["path"])
                if "url" not in asset:
                    print(f"uploading {asset['path']}")
                    asset["url"] = self.space.upload(
                        local_path, asset["path"], expires=asset["expires"]
                    )
                self.assets[name] = asset

    def render(self, **context):
        """
        Load dispatch with context
        """
        # add config to context
        context["_config"] = CONFIG

        # load path + update Jinja context
        data = self._load_path_data(self.path)
        context.update(data)

        # render meta + update Jinja context
        meta_yaml = data["_meta_tmpl"].render(**context)
        data["meta"] = yaml.safe_load(meta_yaml)
        context.update(data)

        # load assets
        self.upload_assets(data["meta"].get("assets", []))
        data["assets"] = self.assets
        context.update(data)

        # render markdown + update jinja context
        data["md"] = data["_content_tmpl"].render(**context)
        data["html"] = utils.md_to_html(data["md"])
        context.update(data)

        # load template
        tmpl_name = data["meta"].get("template", None)
        if tmpl_name is not None:
            tmpl_path = os.path.join(TEMPLATES_DIR, f"{tmpl_name}.html")
            if not os.path.exists(tmpl_path):
                raise exc.ConfigError(f"Template path: {tmpl_path} does not exist.")
            tmpl = Template(open(tmpl_path).read())
            data["html"] = tmpl.render(**context)

        # convert html to text
        data["text"] = utils.html_to_text(data["html"])
        return data


class PRM(object):
    """
    Person + Press + Artist Relatitonship Management
    TODO: abstract out by platform.
    """

    PEOPLE_CACHE_FILE = "contact.json"

    mailer = platform.Email()

    def __init__(self, **kwargs):

        # people configurations
        self.people_url = kwargs.get("people_url", PRM_PEOPLE_URL)
        self.people_platform = kwargs.get(
            "people_host_platform", PRM_PEOPLE_HOST_PLATFORM
        )

        # connection cache
        self._conn = None

    @property
    def conn(self):
        if self._conn is None:
            if self.people_platform == "notion":
                self._conn = platform.Notion()
            # TODO
            else:
                raise NotImplementedError(
                    f"Invalid people_platform_host: {self.people_platform}"
                )
        return self._conn

    def get_people(self):
        """"""
        return reversed(list(self.conn.get_dataset(self.people_url)))

    def send_dispatch(self, path):
        """"""
        disp = Dispatch(path)
        for i, person in enumerate(self.get_people(), start=1):
            data = disp.render(**person)
            if data["meta"]["email"]["to_email"][0]["addr"] not in SENT_TO_EMAILS:
                try:
                    self.mailer.send(
                        subject=data["meta"]["email"].get("subject", ""),
                        from_email=data["meta"]["email"].get("from_email", []),
                        to_email=data["meta"]["email"].get("to_email", []),
                        html=data["html"],
                        text=data["text"],
                    )
                    print(f"{data['meta']['email']['to_email'][0]}")
                    time.sleep(2)
                except Exception as e:
                    d = data["meta"]["email"]["to_email"][0]
                    d["error"] = True
                    d["message"] = str(e)
                    print(f"{d}")
            else:
                print("." * random.choice([1, 2, 3, 4]))
