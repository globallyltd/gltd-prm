from gltd_prm.tests.core import GltdPrmTest
from gltd_prm.core import Dispatch


class TestDispatch(GltdPrmTest):
    def test_dispatch_md_parse(self):
        md_path = self.get_fixture(
            "2020-12-09_gltd-cat-1_malik-alston-i-need-you-ep.md"
        )
        disp = Dispatch(md_path)
        data = disp._parse_path(md_path)
        self.assertEqual(data["year"], 2020)
        self.assertEqual(data["month"], 12)
        self.assertEqual(data["day"], 9)
        self.assertEqual(data["release_num"], 1)
        self.assertEqual(data["cat_num"], "gltd.cat/1")
        self.assertEqual(data["slug"], "malik-alston-i-need-you-ep")

    def test_dispatch_send(self):
        md_path = self.get_fixture(
            "2020-12-09_gltd-cat-1_malik-alston-i-need-you-ep.md"
        )
        disp = Dispatch(md_path)
        disp = disp.render(
            **{
                "name_first": "Brian",
                "name_last": "Abelson",
                "email_main": "brianabelson@gmail.com",
            }
        )
        assert "Brian" in disp["html"]
