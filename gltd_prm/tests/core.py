import os

from unittest import TestCase

from gltd_prm import utils

FIXTURES_DIR = utils.here(__file__, "fixtures/")


class GltdPrmTest(TestCase):
    def get_fixture(self, path):
        return os.path.join(FIXTURES_DIR, path)
