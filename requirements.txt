attrs==20.3.0
boto3==1.16.35
botocore==1.19.35
certifi==2020.12.5
chardet==3.0.4
commonmark==0.9.1
dictdiffer==0.8.1
dominate==2.5.2
-e git+https://gitlab.com/gltd/gltd-prm.git@ea5f0bb3513c52b2497f7409ea8698ffa7edd6a8#egg=gltd_prm
idna==2.10
iniconfig==1.1.1
Jinja2==2.11.2
jmespath==0.10.0
Markdown==3.3.3
MarkupSafe==1.1.1
mistletoe==0.7.2
notion-py==0.0.6
packaging==20.8
pluggy==0.13.1
py==1.10.0
pyparsing==2.4.7
pytest==6.2.0
python-dateutil==2.8.1
python-dotenv==0.15.0
python-slugify==4.0.1
pytz==2020.4
PyYAML==5.3.1
requests==2.24.0
s3transfer==0.3.3
six==1.15.0
text-unidecode==1.3
toml==0.10.2
tzlocal==2.1
urllib3==1.25.11
