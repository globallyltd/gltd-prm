# gltd-prm
==========
Person|Press|Producer Relationship Manager using Notion + custom python code. 



## installation
    - Clone the repository, create a virtual python environment and run `pip3 install -e .`
    - **NOTE**: If using protonmail, setup [proton mail bridge](https://protonmail.com/bridge/install) for local imap/smtp access. TOOO: installation on linux.


## configuration
    - See [config.yml](./config.yml) and [.sample.env](.sample.env) for instructions.


## todo
-[] import activity from email + sync with notion
